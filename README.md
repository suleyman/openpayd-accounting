# Openpayd Accounting

Openpayd Accounting test case.

## Installation

App needs listed apps and tools to be installed.
* Jdk8, Maven, PostgreSql, Tomcat Server(v9)
* Create database named `accounting` on your postgre sql instance.

### Building

```sh 
$ mvn clean package -Ddb.connectionURL=jdbc:postgresql://[POSTGRESQL_HOST]:5432/accounting -Ddb.username=[POSTGRESQL_USER] -Ddb.password=[POSTGRESQL_PASSWORD]

```

- Copy `target/ROOT.war` to your respective Tomcat webbapps directory.
- Start Tomcat

## Usage

- See Javadocs at `target/apidocs/index.html`
- Import `postman_collection.json` to Postman app.
- Import `postman_environment.json` to Postman app.
- Run `Test case 1` with given environment and colleciton on `Collection Runner`

## Credits

- *Suleyman ŞAHİN*