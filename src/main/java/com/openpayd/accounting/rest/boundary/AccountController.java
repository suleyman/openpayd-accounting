package com.openpayd.accounting.rest.boundary;

import com.openpayd.accounting.business.boundary.AccountService;
import com.openpayd.accounting.business.boundary.ClientService;
import com.openpayd.accounting.business.boundary.TransactionService;
import com.openpayd.accounting.business.entity.Account;
import com.openpayd.accounting.business.entity.Client;
import com.openpayd.accounting.business.entity.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/clients/{clientId}/accounts")
@Validated
public class AccountController {

    @Autowired
    private ClientService clientService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private TransactionService transactionService;

    @GetMapping
    public List<Account> getClientAccounts(@PathVariable Long clientId) {
        clientService.checkExists(clientId);
        return accountService.findClientAccounts(clientId);
    }

    @PostMapping
    public Account createAccount(@PathVariable Long clientId, @RequestBody @Valid Account account) {
        Client client = clientService.findById(clientId);
        account.setClient(client);
        return accountService.createAccount(account);
    }

    @GetMapping("/{accountId}")
    public Account findAccount(@PathVariable Long clientId, @PathVariable Long accountId) {
        clientService.checkExists(clientId);
        return accountService.findById(accountId);
    }

    @GetMapping("/{accountId}/transactions")
    public List<Transaction> getAccountTransactions(@PathVariable Long clientId, @PathVariable Long accountId) {
        clientService.checkExists(clientId);
        accountService.checkExists(accountId);
        return transactionService.getAccountTransactions(accountId);
    }

}
