package com.openpayd.accounting.business.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The type Account.
 */
@Data
@Entity
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(nullable = false, updatable = false, unique = true)
    private Long id;

    @ManyToOne
    @JsonIgnore
    private Client client;

    @Column(nullable = false, precision = 8, columnDefinition="DECIMAL(8,4)")
    private BigDecimal balance = BigDecimal.ZERO;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "Account type cannot be empty")
    private AccountType accountType;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "Balance status cannot be empty")
    private BalanceStatus balanceStatus = BalanceStatus.CREDIT;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false, updatable = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date created = new Date();

    /**
     * Instantiates a new Account.
     */
    public Account() {
    }

    /**
     * Instantiates a new Account.
     *
     * @param balance       the balance
     * @param accountType   the account type
     * @param balanceStatus the balance status
     * @param created       the created
     */
    public Account(BigDecimal balance, AccountType accountType, BalanceStatus balanceStatus, Date created) {
        this.balance = balance;
        this.accountType = accountType;
        this.balanceStatus = balanceStatus;
        this.created = created;
    }

}
