package com.openpayd.accounting.business.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The type Transaction.
 */
@Data
@Entity
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne
    private Account debitAccount;

    @ManyToOne
    private Account creditAccount;

    @Column(nullable = false, precision = 8, columnDefinition="DECIMAL(8,4)")
    private BigDecimal amount;

    private String message;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false, updatable = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date created = new Date();

    /**
     * Instantiates a new Transaction.
     */
    public Transaction() {
    }

    /**
     * Instantiates a new Transaction.
     *
     * @param debitAccount  the debit account
     * @param creditAccount the credit account
     * @param amount        the amount
     * @param message       the message
     */
    public Transaction(Account debitAccount, Account creditAccount, BigDecimal amount, String message) {
        this.debitAccount = debitAccount;
        this.creditAccount = creditAccount;
        this.amount = amount;
        this.message = message;
    }

}
