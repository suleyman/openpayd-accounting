package com.openpayd.accounting.business.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

/**
 * The type Address.
 */
@Data
@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @JsonIgnore
    private Long id;

    @NotEmpty(message = "Addressline cannot be empty")
    @Column(nullable = false)
    private String addressLine1;

    private String addressLine2;

    @NotEmpty(message = "City cannot be empty")
    @Column(nullable = false)
    private String city;

    @NotEmpty(message = "Country cannot be empty")
    @Column(nullable = false)
    private String country;

    /**
     * Instantiates a new Address.
     */
    public Address() {
    }

    /**
     * Instantiates a new Address.
     *
     * @param addressLine1 the address line 1
     * @param addressLine2 the address line 2
     * @param city         the city
     * @param country      the country
     */
    public Address(String addressLine1, String addressLine2, String city, String country) {
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.country = country;
    }

}
